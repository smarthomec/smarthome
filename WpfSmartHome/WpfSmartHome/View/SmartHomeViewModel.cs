﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using WpfSmartHome.Modele;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace WpfSmartHome
{
    public class SmartHomeViewModel
    {
        public int nombre = 0;

        public string DureeNuit;
        public string Nombre { get; set; }
        public string NomApplication { get; set; }
        public IEnumerable<Capteur> CapteurList { get; set; }
        public Capteur CapteurSelect { get; set; }
        public DateTime dateStart { get; set; }
        public DateTime dateEnd { get; set; }
        public PlotModel MyModel { get; set; }
        Dictionary<string, Capteur> dic = new Dictionary<string, Capteur>();

        public SmartHomeViewModel()
        {
            CapteurSelect = null;
            MyModel = new PlotModel();
            NomApplication = "Application Smart Home";
            CapteurList = LireFichierXml(@"..\..\..\Smarthome\ecole\capteurs.xtim");
            initListCapt();
            LireTousLesFichierDT(@"..\..\..\Smarthome\ecole\netatmo");
            Nombre = nombre+" données chargées";
            dateStart = new DateTime(2014, 02, 08);
            dateEnd = new DateTime(2014, 02, 10);
            DureeNuit = "Durée de la nuit : ";
        }


        public void initListCapt()
        {
            XDocument xdoc = XDocument.Load(@"..\..\..\Smarthome\ecole\capteurs.xtim");
            XElement xdocattributs = xdoc.Root;
            if (xdocattributs != null)
            {
                IEnumerable<XElement> xCapteurList = xdocattributs.Elements("capteur");
                foreach (XElement xCapteur in xCapteurList)
                {
                    string unNom = xCapteur.Element("id").Value;
                    var unCapteur = new Capteur(unNom)
                    {
                        description = xCapteur.Element("description").Value,
                        lieu = xCapteur.Element("lieu").Value
                    };

                    var box = xCapteur.Element("box").Value;
                    var xGrandeur = xCapteur.Element("grandeur");
                    var nomGrand = xGrandeur.Attribute("nom").Value;
                    var unitéGrand = xGrandeur.Attribute("unite").Value;
                    var abrevGrand = xGrandeur.Attribute("abreviation").Value;
                    unCapteur.grandeur = new Grandeur(nomGrand, unitéGrand, abrevGrand);
                    if (box.Contains("netatmo"))
                        dic.Add(unCapteur.nom, unCapteur);
                }
            }
        }
        public static IEnumerable<Capteur> LireFichierXml(string strFichier)
        {
            XDocument xdoc = XDocument.Load(strFichier);
            XElement xdocattributs = xdoc.Root;
            if (xdocattributs != null)
            {
                IEnumerable<XElement> xCapteurList = xdocattributs.Elements("capteur");
                foreach (XElement xCapteur in xCapteurList)
                {
                    string unNom = xCapteur.Element("id").Value;
                    var unCapteur = new Capteur(unNom)
                    {
                        description = xCapteur.Element("description").Value,
                        lieu = xCapteur.Element("lieu").Value
                    };
                    var xGrandeur = xCapteur.Element("grandeur");
                    var box = xCapteur.Element("box").Value;
                    var nomGrand = xGrandeur.Attribute("nom").Value;
                    var unitéGrand = xGrandeur.Attribute("unite").Value;
                    var abrevGrand = xGrandeur.Attribute("abreviation").Value;
                    unCapteur.grandeur = new Grandeur(nomGrand, unitéGrand, abrevGrand);
                    if (box.Contains("netatmo"))
                        yield return unCapteur;
                }
            }
        }
        public void LireTousLesFichierDT(string strRepertoire)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(strRepertoire);
            FileInfo[] tabloFichier = dirInfo.GetFiles("*.dt");
            foreach (FileInfo unFichier in tabloFichier)
               LireFichierDT(unFichier.FullName);
        }

        public void LireFichierDT(string strFichier)
        {
            String[] tablo = File.ReadAllLines(strFichier);
            foreach (string sLigne in tablo)
            {
                string s = sLigne;
                string strDate = s.Substring(1, 19);
                s = s.Substring(22);
                int positionEspace = s.IndexOf(' ');
                string strCapteur = s.Substring(0, positionEspace);
                string strValeur = s.Substring(positionEspace + 1);
                DateTime uneDate = DateTime.Parse(strDate);
                Capteur unCapteur = null;
                if (dic.TryGetValue(strCapteur, out unCapteur))
                {
                    double uneValeur = double.Parse(strValeur);
                    Mesure m = new Mesure(unCapteur, uneDate, uneValeur);
                    unCapteur.ajout(m);
                    nombre++;
                }
            }
        }//LireFichierDT

        public void setUpModel()
        {
            DateTimeAxis xAxis = new DateTimeAxis
            {
                Position = AxisPosition.Bottom,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,
            };
            
            if ((dateEnd - dateStart).TotalDays == 1 )
            {
                xAxis.StringFormat = "HH:mm";
                xAxis.IntervalType = DateTimeIntervalType.Hours;
            }
            else if ((dateEnd - dateStart).TotalDays == 2)
            {
                xAxis.StringFormat = "HH dd/MM";
                xAxis.IntervalType = DateTimeIntervalType.Hours;
            }
            else
            {
                xAxis.StringFormat = "dd/MM/yyyy";
                xAxis.IntervalType = DateTimeIntervalType.Days;
                xAxis.MinorIntervalType = DateTimeIntervalType.Days;
            }

            MyModel.Axes.Add(xAxis);
            MyModel.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = CapteurSelect.grandeur.nom, Unit = CapteurSelect.grandeur.abreviation });
        }//setUpModel()

        public void loadData()
        {
            var fs = new LineSeries()
            {
                StrokeThickness = 2,
                MarkerSize = 3,
                CanTrackerInterpolatePoints = false,
                Smooth = true
            };
            var fsColor = new LineSeries()
            {
                StrokeThickness = 2,
                MarkerSize = 3,
                CanTrackerInterpolatePoints = false,
                Smooth = true,
                Color = OxyColors.Red
            };
            if ((dateEnd - dateStart).TotalDays == 2)
            {
                Capteur capt = null;
                if (dic.TryGetValue(CapteurSelect.nom, out capt))
                {
                    SleepTime sleepTime = null;
                    if (CapteurSelect.nom == "co2chambrebeatrice" || CapteurSelect.nom == "co2chambrealain")
                    {
                        sleepTime = new SleepTime(capt, dateStart);
                    }
                    foreach (var mesure in capt.mesuresList)
                    {
                        if (mesure.datetime >= dateStart && mesure.datetime <= dateEnd)
                        {
                            if(sleepTime != null) {
                                if ((mesure.datetime > sleepTime.SleepStart) && (mesure.datetime < sleepTime.SleepEnd))
                                {
                                   // Console.WriteLine("test");
                                    fsColor.Points.Add(new DataPoint(DateTimeAxis.ToDouble(mesure.datetime), mesure.valeur));
                                }
                            }
                            fs.Points.Add(new DataPoint(DateTimeAxis.ToDouble(mesure.datetime), mesure.valeur));
                        }
                    }
                }
                
                MyModel.Series.Add(fs);
                MyModel.Series.Add(fsColor);

            }
            else
            {
                Capteur capt = null;
                if (dic.TryGetValue(CapteurSelect.nom, out capt))
                {
                    foreach (var mesure in capt.mesuresList)
                    {
                        if (mesure.datetime >= dateStart && mesure.datetime <= dateEnd)
                        {
                            fs.Points.Add(new DataPoint(DateTimeAxis.ToDouble(mesure.datetime), mesure.valeur));
                        }
                    }
                }
                MyModel.Series.Add(fs);
            }
            
        }//loadData

        public void dateDebut_SelectedDateChanged()
        {
            if (CapteurSelect != null)
                updateGraph();
        }

        public void dateFin_SelectedDateChanged()
        {
            if (CapteurSelect != null)
                updateGraph();
        }

        public void updateGraph()
        {
            MyModel.Series.Clear();
            MyModel.Axes.Clear();

            setUpModel();
            loadData();

            MyModel.InvalidatePlot(true);
        }
    }
}