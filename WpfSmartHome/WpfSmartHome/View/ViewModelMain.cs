﻿using System;
using System.Collections.ObjectModel;
using WpfSmartHome.Modele;
using OxyPlot;

namespace WpfSmartHome.View
{
    public class ViewModelMain : ViewModelBase
    {
        private PlotModel plotModel;
        public PlotModel PlotModel
        {
            get { return plotModel; }
            set { plotModel = value; OnPropertyChanged("PlotModel"); }
        }

        public ViewModelMain()
        {
            SmartHomeViewModel smart = new SmartHomeViewModel();
        }

        private string nomApplication;
        public string NomApplication
        {
            get { return nomApplication; }
            set
            {
                nomApplication = value;
                OnPropertyChanged("NomApplication");
            }
        }
        private string dureeNuit;
        public string DureeNuit
        {
            get { return dureeNuit; }
            set
            {
                dureeNuit = value;
                OnPropertyChanged("DureeNuit");
            }
        }

    }
}
