﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfSmartHome
{
    public partial class App : Application
    {
        public static SmartHomeViewModel viewModelSmartHome{ get; set; }

        public App()
        {
            viewModelSmartHome = new SmartHomeViewModel();
        }
    }
}
