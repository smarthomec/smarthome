﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfSmartHome.Modele
{
    public class Mesure
    {
        private DateTime date;
        private double uneValeur;

        public DateTime datetime { get; set; }
        public double valeur { get; set; }
        public string id { get; set; }
        public Capteur capteur { get; set; }

        public Mesure(string uneId, DateTime uneDate, double uneValeur)
        {
            valeur = uneValeur;
            datetime = uneDate;
            id = uneId;
        }

        public Mesure(Capteur unCapteur, DateTime uneDate, double uneValeur)
        {
            valeur = uneValeur;
            datetime = uneDate;
            capteur = unCapteur;
        }
        public Mesure(DateTime date, double uneValeur)
        {
            this.date = date;
            this.uneValeur = uneValeur;
        }
        /*public Mesure()
{

}
public Mesure(DateTime _datetime, double _valeur)
{
   datetime = _datetime;
   valeur = _valeur;
}

public override string ToString()
{
   return base.ToString();
}*/
    }
}
