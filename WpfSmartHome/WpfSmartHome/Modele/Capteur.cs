﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfSmartHome.Modele
{
    public class Capteur
    {
        public string nom { get; set; }
        public List<Mesure> mesuresList { get; set; }
        public string lieu { get; set; }
        public string description { get; set; }
        public Grandeur grandeur { get; set; }
        public int taille { get { return mesuresList.Count; } }
        
        public Capteur(string unNom)
        {
            nom = unNom;
            mesuresList = new List<Mesure>();
        }

        public void ajout(Mesure mesure)
        {
            mesuresList.Add(mesure);
        }
    }

    public class Grandeur
    {
        public String nom { get; set; }
        public String unite { get; set; }
        public String abreviation { get; set; }

        public Grandeur(String _nom, String _unite, String _abreviation)
        {
            nom = _nom;
            unite = _unite;
            abreviation = _abreviation;
        }
    }
}
