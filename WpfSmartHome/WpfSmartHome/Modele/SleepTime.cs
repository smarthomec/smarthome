﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfSmartHome.Modele
{
    class SleepTime
    {
        public Capteur Capteur { get; set; }
        public double Moyenne { get; set; }
        public DateTime SleepStart { get; set; }
        public DateTime SleepEnd { get; set; }
        public TimeSpan TimeToSleep { get; set; }
        private List<Mesure> Periode { get; set; }
        private int numberOfData { get; set; }

    public SleepTime(Capteur c, DateTime dateStart)
        {
            this.Capteur = c;
            this.Periode = new List<Mesure>();
            DateTime StartPeriod;
            DateTime EndPeriod;
            numberOfData = 0;
            double moyenne;
            StartPeriod = dateStart.Date.AddHours(12);
            EndPeriod = dateStart.Date.AddDays(1).AddHours(12);
            
            foreach (Mesure mesure in Capteur.mesuresList)
            {
                if((mesure.datetime > StartPeriod) && (mesure.datetime < EndPeriod))
                {
                    Periode.Add(mesure);
                    numberOfData++;
                }
            }

            this.findSleepStart();
            moyenne = calculMoyenne();
            Console.WriteLine( "Moyenne : " +moyenne);
            //findSleepStartAndEnd(dateStart);
        }

        
        public double calculMoyenne()
        {
            double moyenne = 0;
            int cpt = 0;
            foreach (var mesure in this.Capteur.mesuresList)
            {
                cpt++;
                moyenne = moyenne + mesure.valeur;
            }
            return moyenne /= cpt;
        }

        private void findSleepStart()
        {
            Double val;
            
            for(int i = 0; i < numberOfData; i++)
            {
                if (i+ 36 < numberOfData)
                {
                    val = Periode[i].valeur + ((Periode[i].valeur /100)*50);
                    if (Periode[i+36].valeur > val)
                    {
                        SleepStart = Periode[i].datetime;
                        this.findSleepEnd(i);
                        break; 
                    }
                }
                else
                {
                    break;
                }
            }
        }

        private void findSleepEnd(int i)
        {
            Double val;

            for (; i < numberOfData; i++)
            {
                if (i + 24 < numberOfData)
                {
                    val = Periode[i].valeur - ((Periode[i].valeur / 100) * 30);
                    //Console.WriteLine("valeur : " + Periode[i].valeur + " val " + val);
                    if (Periode[i + 24].valeur < val)
                    {
                        SleepEnd = Periode[i].datetime;
                        this.getTimeSleep();
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }

        private void getTimeSleep()
        {
            TimeToSleep =  SleepEnd - SleepStart;
            Console.WriteLine("Nombre d'heures : " + TimeToSleep.TotalHours);
            Console.WriteLine("Nombre de minutes : " + TimeToSleep.TotalMinutes);

        }

        private List<DateTime> SortAscending(List<DateTime> list)
        {
            list.Sort((a, b) => a.CompareTo(b));
            return list;
        }
    }
}
