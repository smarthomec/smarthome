﻿using System.Windows;
using System.Windows.Controls;

namespace WpfSmartHome
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = App.viewModelSmartHome;
        }

        private void dateDebut_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            App.viewModelSmartHome.dateDebut_SelectedDateChanged();
        }

        private void dateFin_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            App.viewModelSmartHome.dateFin_SelectedDateChanged();
        }
        private void on_change(object sender, SelectionChangedEventArgs e)
        {
            App.viewModelSmartHome.updateGraph();
        }
    }
}
